/**********************************************************************************
 * Program		: onyxxwd
 * File			: onyxxwd.c
 * Copyright		: Copyright (c) 2016 LynxSpring LLC - All Rights Reserved
 * Written by		: Bryan Wilcutt
 * Date 		: 6/2/2016
 *
 * Description		:
 *
 * This daemon provides facility to feed the BeagleBone watch dog timer and to keep
 * statistics on the time-to-live stat as well as the number of crash-reboots.
 **********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/watchdog.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/resource.h>

#define TTL_UPDATE (15 * 60) /* Update TTL every x mins */
#define WD_UPDATE  5 /* Update watchdog every x seconds */
#define WD_TIMEOUT 15 /* Watchdog set in seconds, must be multiples of WD_UPDATE */

#define WD_FILE 			"/dev/watchdog"
#define TTL_FILE 			"/opt/stats/.ttl"
#define CRASH_BOOT_COUNTER 	"/opt/stats/.cbc"
#define CRASH_BOOT 			"/opt/stats/.cb"
#define REBOOTFILE			"/opt/stats/.rb"
#define LOG_FILE_DIR    	"/var/log"
#define LOG_FILE_CB     	"/opt/stats/logs"

static int ttl_update = TTL_UPDATE;
static int wd_update = WD_UPDATE;
static int wd_timeout = WD_TIMEOUT;

void logSave()
{
	char cmd[100];

	/* Save current logs if a CB is detected. */

	sprintf(cmd, "cp %s/* %s", LOG_FILE_DIR, LOG_FILE_CB);
	system(cmd);
}

int main(int argc, char *argv[])
{
	FILE *fpcb = NULL;
	FILE *fpttl = NULL;
	int fpwd = 0;
	char buf[20];
	unsigned int i, cb_count;
	int ttl_timer;
	unsigned long ttl;

	/* This task must run at a LOW priority so as not to intefer with runtime operations. */

	setpriority(PRIO_PROCESS, 0, 15);

	/* Requires 3 arguments: ttl_update wd_update wd_timeout */

	if (argc != 4)
	{
		printf("\nONYXXWD Usage: OnyxxWD time-alive updates (mins) feed_the_dog_update (seconds) watchdog_timeout (seconds)\n");
		exit(0);
	} else {
		ttl_update = atoi(argv[1]) * 60;
		wd_update = atoi(argv[2]);
		wd_timeout = atoi(argv[3]);
	}

	memset(buf, 0, sizeof(buf));

	/* Increment the boot counter file */

	i = 1;

	if ((fpcb = fopen(REBOOTFILE, "r")) != NULL)
	{
		if (fgets(buf, sizeof(buf), fpcb))
		{
			i = atoi(buf) + 1;
		}

		fclose(fpcb);
	}

	if ((fpcb = fopen(REBOOTFILE, "w")) != NULL)
	{
		fprintf(fpcb, "%i", i);
		fclose(fpcb);
	}

	/* If the CRASH_BOOT file exists, we did not close gracefully and therefore counts as a reboot due to system crash.
	 * This also indicates we must store the existing logs to our special level1, level2 or level3 directories. */

	if ((fpcb = fopen(CRASH_BOOT, "r")) == NULL)
	{
		/* No crash boot file.  Create one here. When exiting gracefully, it should be deleted */

		if ((fpcb = fopen(CRASH_BOOT, "w")) != NULL)
		{
			fwrite(buf, 1, 1,  fpcb);
			fclose(fpcb);
		}
	} else {
		fclose(fpcb);

		/* Save logs */

		logSave();

		/* Crash boot detected.  Increment crash boot counter. */

		buf[0] = 0;

		if ((fpcb = fopen(CRASH_BOOT_COUNTER, "r")) != NULL)
		{
			/* Read last value */

			buf[0] = 0;
			fgets(buf, sizeof(buf) - 1, fpcb);
			fclose(fpcb);
		}

		/* Update to new value */

		if ((fpcb = fopen(CRASH_BOOT_COUNTER, "w")) != NULL)
		{
			cb_count = atoi(buf) + 1;

			fprintf(fpcb, "%i", cb_count);
			fclose(fpcb);
		}
	}

	/* Open TTL file and read last-known-setting which is the number of seconds the
	 * unit has been powered.
	 */

	buf[0] = 0;
	if ((fpttl = fopen(TTL_FILE, "r")) != NULL)
	{
		/* Read last setting */

		fgets(buf, sizeof(buf) - 1, fpttl);
		fclose(fpttl);
	}

	ttl = atol(buf);
	fpttl = fopen(TTL_FILE, "w+");

	/* Start the watchdog timer here, defaults to 60 seconds time out */

	if ((fpwd = open(WD_FILE, O_WRONLY, S_IWRITE)))
	{
		ioctl(fpwd, WDIOC_SETTIMEOUT, &wd_timeout);   /* Send time request to the driver. */
	}

	/* Loop forever.  Wake up periodically to feed the dog and to update the
	 * TTL in the ttl file.
	 */

	while (1)
	{
		if (fpwd)
		{
			write(fpwd, "\n", 1); /* Feed the dog */
			fsync(fpwd);
		}

		sleep(wd_update);

		ttl_timer += wd_update;

		if (ttl_timer >= (ttl_update / wd_update))
		{
			ttl += ttl_timer;
			ttl_timer = 0;

			if (fpttl)
			{
				fprintf(fpttl, "%lu", ttl);
				fflush(fpttl);
				rewind(fpttl);
			}
		}
	}

	return 0;
}
